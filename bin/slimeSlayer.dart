// ignore: duplicate_ignore
// ignore_for_file: camel_case_types, duplicate_ignore, file_names

import 'dart:io';
import 'dart:math';
import 'package:console/console.dart';
import 'dart:async';

mixin GameHelper {
  Random random = Random();

  int randomInRange(int min, int max) {
    return min + random.nextInt(max - min);
  }

  bool randomByPercentage(int percentage) {
    List<int> percentLst = [];
    int limit1 = percentage;
    int limit0 = 100 - percentage;
    for (int i = 0; i < 100; i++) {
      late int value;
      if (limit0 > 0 && limit1 > 0) {
        value = randomInRange(0, 2);
      } else if (limit0 == 0 && limit1 > 0) {
        value = 1;
      } else if (limit1 == 0 && limit0 > 0) {
        value = 0;
      }

      if (value == 1) {
        limit1--;
      } else {
        limit0--;
      }
      percentLst.add(value);
    }
    int isTrue = randomInRange(0, 100);
    if (percentLst[isTrue] == 1) {
      return true;
    } else {
      return false;
    }
  }

  void printYellow(String text) {
    print('\x1B[33m$text\x1B[0m');
  }

  void printGreen(String text) {
    print('\x1b[32m$text\x1B[0m');
  }

  void printRed(String text) {
    print('\x1b[31m$text\x1B[0m');
  }

  void printBlue(String text) {
    print('\x1b[34m$text\x1B[0m');
  }
}

class criticalStrike {
  int critical() {
    int x = 0;
    return x;
  }
}

class stat {
  void getStat() {}
}

class advantage {
  bool isAdvantage(String mapElement, String element) {
    if (mapElement == element) {
      return true;
    }
    return false;
  }
}

class buff with GameHelper {
  bool restoreHpDrop() {
    bool isDrop = randomByPercentage(50);
    if (isDrop == true) {
      return true;
    }
    return false;
  }
}

abstract class mabObject {
  int _posX = 0;
  int _posY = 0;
  var _mapSymbol = '';

  int getPosX() {
    return _posX;
  }

  void setPosX(int posX) {
    _posX = posX;
  }

  int getPosY() {
    return _posY;
  }

  void setPosY(int posY) {
    _posY = posY;
  }

  getMapSymbol() {
    return _mapSymbol;
  }
}

class healingPotion extends mabObject {
  healingPotion(posX, posY) {
    _posX = posX;
    _posY = posY;
    _mapSymbol = "H";
  }
}

abstract class living extends mabObject with GameHelper {
  String _type = "No type yet";
  int _maxHealthPoint = 0;
  int _healthpoint = 0;
  int _accuracy = 0; // Convert to percentage
  int _phyRes = 0;
  int _magicRes = 0;
  int _basicAtkSpd = 0;
  int _criticalChance = 0;

  int getCriticalChance() {
    return _criticalChance;
  }

  String getType() {
    return _type;
  }

  int getMaxHealthPoint() {
    return _maxHealthPoint;
  }

  void setMaxHealthPoint(int hp) {
    _maxHealthPoint = hp;
  }

  int getHealthpoint() {
    return _healthpoint;
  }

  int getAccuracy() {
    return _accuracy;
  }

  int getPhyRes() {
    return _phyRes;
  }

  int getMagicRes() {
    return _magicRes;
  }

  int getBasicAtkSpd() {
    return _basicAtkSpd;
  }

  void setHealthpoint(int healthpoint) {
    _healthpoint = healthpoint;
  }

  void setAccuracy(int accuracy) {
    _accuracy = accuracy;
  }

  void setPhyRes(int phyRes) {
    _phyRes = phyRes;
  }

  void setMagicRes(int magicRes) {
    _magicRes = magicRes;
  }

  void setBasicAtkSpd(int basicAtkSpd) {
    _basicAtkSpd = basicAtkSpd;
  }

  void setCriticalChance(int criticalChance) {
    _criticalChance = criticalChance;
  }

  int usingBasicAtk(int damage, int pen) {
    bool atkMiss = randomByPercentage(_accuracy);
    late int damageDeal;
    if (atkMiss == false) {
      damageDeal = damage + pen;
    } else {
      damageDeal = 0;
    }
    return damageDeal;
  }

  bool isDead() {
    if (_healthpoint <= 0) {
      return true;
    } else {
      return false;
    }
  }
}

abstract class human extends living {
  bool inMap(int x, int y) {
    return (x >= 0 && x < 20) && (y >= 0 && y < 40);
  }

  bool canWalk(int x, int y) {
    return inMap(x, y);
  }

  bool walk(var direction) {
    switch (direction) {
      case 'W':
      case 'w':
        if (walkN()) {
          return false;
        }
        break;
      case 'S':
      case 's':
        if (walkS()) {
          return false;
        }
        break;
      case 'D':
      case 'd':
        if (walkE()) {
          return false;
        }

        break;
      case 'A':
      case 'a':
        if (walkW()) {
          return false;
        }
        break;
      default:
        return false;
    }
    return true;
  }

  bool walkW() {
    if (canWalk(_posX - 1, _posY)) {
      _posX = _posX - 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (canWalk(_posX + 1, _posY)) {
      _posX = _posX + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (canWalk(_posX, _posY + 1)) {
      _posY = _posY + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkN() {
    if (canWalk(_posX, _posY - 1)) {
      _posY = _posY - 1;
    } else {
      return true;
    }
    return false;
  }
}

abstract class slime extends living {
  final _elementList = ['Earth', 'Fire', 'Water'];
  int _magicAtk = 0;
  int _magicPen = 0;
  String _element = "No element";

  int getMagicAtk() {
    return _magicAtk;
  }

  int getMagicPen() {
    return _magicPen;
  }

  String getElement() {
    return _element;
  }

  void setMagicAtk(int magicAtk) {
    _magicAtk = magicAtk;
  }

  void setMagicPen(int magicPen) {
    _magicPen = magicPen;
  }

  void setElement(String element) {
    _element = element;
  }
}

class fighter extends human with GameHelper implements criticalStrike, stat {
  int _phyAtk = 0;
  int _phyPen = 0;

  fighter() {
    _mapSymbol = 'P';
    _type = "Fighter";
    _maxHealthPoint = 20;
    _healthpoint = 20;
    _accuracy = randomInRange(20, 30);
    _phyRes = randomInRange(4, 10);
    _magicRes = randomInRange(4, 10);
    _basicAtkSpd = 2;
    _criticalChance = 20;
    _phyAtk = randomInRange(5, 10);
    _phyPen = randomInRange(2, 5);
  }

  int getPhyAtk() {
    return _phyAtk;
  }

  int getPhyPen() {
    return _phyPen;
  }

  void setPhyAtk(int phyAtk) {
    _phyAtk = phyAtk;
  }

  void setPhyPen(int phyPen) {
    _phyPen = phyPen;
  }

  @override
  void getStat() {
    print("Role : $_type");
    print("Health points : $_healthpoint");
    print("Accuracy : $_accuracy%");
    print("Physical resistance : $_phyRes");
    print("Magic resistance : $_magicRes");
    print("Basic attack speed : $_basicAtkSpd");
    print("Critical chance : $_criticalChance");
    print("Physical attack : $_phyAtk");
    print("Physical penetration : $_phyPen");
  }

  @override
  int critical() {
    int damage = usingBasicAtk(_phyAtk, _phyPen);
    if (damage > 0) {
      bool canDealCritical = randomByPercentage(_criticalChance);
      if (canDealCritical == true) {
        print("Player deal critical strike!");
        damage = damage * 2;
      }
    }
    return damage;
  }
}

class markman extends human with GameHelper implements criticalStrike, stat {
  int _phyAtk = 0;
  int _phyPen = 0;

  markman() {
    _mapSymbol = 'P';
    _type = "Markman";
    _maxHealthPoint = 15;
    _healthpoint = 15;
    _accuracy = randomInRange(20, 35);
    _phyRes = randomInRange(4, 5);
    _magicRes = randomInRange(4, 5);
    _basicAtkSpd = 1;
    _criticalChance = 25;
    _phyAtk = randomInRange(5, 15);
    _phyPen = randomInRange(2, 7);
  }

  int getPhyAtk() {
    return _phyAtk;
  }

  int getPhyPen() {
    return _phyPen;
  }

  void setPhyAtk(int phyAtk) {
    _phyAtk = phyAtk;
  }

  void setPhyPen(int phyPen) {
    _phyPen = phyPen;
  }

  @override
  void getStat() {
    print("Role : $_type");
    print("Health points : $_healthpoint");
    print("Accuracy : $_accuracy%");
    print("Physical resistance : $_phyRes");
    print("Magic resistance : $_magicRes");
    print("Basic attack speed : $_basicAtkSpd");
    print("Critical chance : $_criticalChance");
    print("Physical attack : $_phyAtk");
    print("Physical penetration : $_phyPen");
  }

  @override
  int critical() {
    int damage = usingBasicAtk(_phyAtk, _phyPen);
    if (damage > 0) {
      bool canDealCritical = randomByPercentage(_criticalChance);
      if (canDealCritical == true) {
        print("Player deal critical strike!");
        damage = damage * 2;
      }
    }
    return damage;
  }
}

class mage extends human with GameHelper implements criticalStrike, stat {
  int _magicAtk = 0;
  int _magicPen = 0;

  mage() {
    _mapSymbol = 'P';
    _type = "Mage";
    _maxHealthPoint = 10;
    _healthpoint = 10;
    _accuracy = randomInRange(20, 35);
    _phyRes = randomInRange(4, 5);
    _magicRes = randomInRange(4, 5);
    _basicAtkSpd = 4;
    _criticalChance = 15;
    _magicAtk = randomInRange(10, 15);
    _magicPen = randomInRange(2, 5);
  }

  int getMagicAtk() {
    return _magicAtk;
  }

  int getMagicPen() {
    return _magicPen;
  }

  void setMagicAtk(int magicAtk) {
    _magicAtk = magicAtk;
  }

  void setMagicPen(int magicAtk) {
    _magicAtk = magicAtk;
  }

  @override
  void getStat() {
    print("Role : $_type");
    print("Health points : $_healthpoint");
    print("Accuracy : $_accuracy%");
    print("Physical resistance : $_phyRes");
    print("Magic resistance : $_magicRes");
    print("Basic attack speed : $_basicAtkSpd");
    print("Critical chance : $_criticalChance");
    print("Physical attack : $_magicAtk");
    print("Physical penetration : $_magicPen");
  }

  @override
  int critical() {
    int damage = usingBasicAtk(_magicAtk, _magicPen);
    if (damage > 0) {
      bool canDealCritical = randomByPercentage(_criticalChance);
      if (canDealCritical == true) {
        print("Player deal critical strike!");
        damage = damage * 2;
      }
    }
    return damage;
  }
}

class normalSlime extends slime with GameHelper implements criticalStrike, stat, buff {
  final int _criticalChance = 5;
  normalSlime(int posX, int posY) {
    _posX = posX;
    _posY = posY;
    _mapSymbol = 'N';
    _type = "Normal Slime";
    _healthpoint = 5;
    _element = _elementList[randomInRange(0, 3)];
    _accuracy = randomInRange(20, 40);
    _phyRes = randomInRange(2, 4);
    _magicRes = randomInRange(2, 4);
    _basicAtkSpd = 4;
    _magicAtk = randomInRange(5, 10);
    _magicPen = randomInRange(2, 4);
  }
  @override
  void getStat() {
    print("Role : $_type");
    print("Health points : $_healthpoint");
    print("Element : $_element");
    print("Accuracy : $_accuracy%");
    print("Physical resistance : $_phyRes");
    print("Magic resistance : $_magicRes");
    print("Basic attack speed : $_basicAtkSpd");
    print("Critical chance : $_criticalChance");
    print("Magic attack : $_magicAtk");
    print("Magic penetration : $_magicPen");
  }

  @override
  int critical() {
    int damage = usingBasicAtk(_magicAtk, _magicPen);
    if (damage > 0) {
      bool canDealCritical = randomByPercentage(_criticalChance);
      if (canDealCritical == true) {
        print("Monster deal critical strike!");
        damage = damage * 2;
      }
    }
    return damage;
  }

  @override
  bool restoreHpDrop() {
    bool isDrop = randomByPercentage(30);
    if (isDrop == true) {
      return true;
    }
    return false;
  }
}

class aggressiveSlime extends slime with GameHelper implements criticalStrike, stat, buff {
  final int _criticalChance = 5;
  aggressiveSlime(int posX, int posY) {
    _posX = posX;
    _posY = posY;
    _mapSymbol = 'A';
    _type = "Aggressive Slime";
    _healthpoint = 10;
    _element = _elementList[randomInRange(0, 3)];
    _accuracy = randomInRange(20, 45);
    _phyRes = randomInRange(2, 8);
    _magicRes = randomInRange(2, 8);
    _basicAtkSpd = 5;
    _magicAtk = randomInRange(5, 15);
    _magicPen = randomInRange(2, 8);
  }
  @override
  void getStat() {
    print("Role : $_type");
    print("Health points : $_healthpoint");
    print("Element : $_element");
    print("Accuracy : $_accuracy%");
    print("Physical resistance : $_phyRes");
    print("Magic resistance : $_magicRes");
    print("Basic attack speed : $_basicAtkSpd");
    print("Critical chance : $_criticalChance");
    print("Magic attack : $_magicAtk");
    print("Magic penetration : $_magicPen");
  }

  @override
  int critical() {
    int damage = usingBasicAtk(_magicAtk, _magicPen);
    if (damage > 0) {
      bool canDealCritical = randomByPercentage(_criticalChance);
      if (canDealCritical == true) {
        print("Monster deal critical strike!");
        damage = damage * 2;
      }
    }
    return damage;
  }

  @override
  bool restoreHpDrop() {
    bool isDrop = randomByPercentage(60);
    if (isDrop == true) {
      return true;
    }
    return false;
  }
}

class kingSlime extends slime with GameHelper implements criticalStrike, stat, buff {
  final int _criticalChance = 10;
  kingSlime(int posX, int posY) {
    _posX = posX;
    _posY = posY;
    _mapSymbol = 'B';
    _type = "King Slime";
    _healthpoint = 20;
    _element = _elementList[randomInRange(0, 3)];
    _accuracy = randomInRange(30, 55);
    _phyRes = randomInRange(7, 10);
    _magicRes = randomInRange(7, 10);
    _basicAtkSpd = 4;
    _magicAtk = randomInRange(10, 15);
    _magicPen = randomInRange(5, 10);
  }

  @override
  void getStat() {
    print("Role : $_type");
    print("Health points : $_healthpoint");
    print("Element : $_element");
    print("Accuracy : $_accuracy%");
    print("Physical resistance : $_phyRes");
    print("Magic resistance : $_magicRes");
    print("Basic attack speed : $_basicAtkSpd");
    print("Critical chance : $_criticalChance");
    print("Magic attack : $_magicAtk");
    print("Magic penetration : $_magicPen");
  }

  @override
  int critical() {
    int damage = usingBasicAtk(_magicAtk, _magicPen);
    if (damage > 0) {
      bool canDealCritical = randomByPercentage(_criticalChance);
      if (canDealCritical == true) {
        print("Monster deal critical strike!");
        damage = damage * 2;
      }
    }
    return damage;
  }

  @override
  bool restoreHpDrop() {
    bool isDrop = randomByPercentage(100);
    if (isDrop == true) {
      return true;
    }
    return false;
  }
}

// ignore: camel_case_types
class map with GameHelper implements advantage {
  int width = 20;
  int height = 40;
  String mapElement = "";
  List<normalSlime> nSlimelist = [];
  List<aggressiveSlime> aSlimelist = [];
  List<healingPotion> healingPotions = [];
  late kingSlime boss;
  late int playerPosX;
  late int playerPosY;
  bool approachMonster = false;
  final _elementList = ['Earth', 'Fire', 'Water'];
  // ignore: prefer_typing_uninitialized_variables
  var arrayMap;

  map() {
    int tempPosX = randomInRange(0, 20);
    int tempPosY = randomInRange(0, 4);
    mapElement = _elementList[randomInRange(0, 3)];
    arrayMap = List.generate(
        width, (i) => List.filled(height, '-', growable: false),
        growable: false);
    boss = kingSlime(tempPosX, tempPosY);
    arrayMap[tempPosX][tempPosY] = boss.getMapSymbol();
    for (int i = 0; i < 5; i++) {
      bool exit = false;
      while (exit == false) {
        int tempPosX = randomInRange(0, 20);
        int tempPosY = randomInRange(0, 40);
        if (arrayMap[tempPosX][tempPosY] == '-') {
          nSlimelist.add(normalSlime(tempPosX, tempPosY));
        }
        arrayMap[tempPosX][tempPosY] = nSlimelist[i].getMapSymbol();
        exit = true;
      }
    }
    for (int i = 0; i < 5; i++) {
      bool exit = false;
      while (exit == false) {
        int tempPosX = randomInRange(0, 20);
        int tempPosY = randomInRange(0, 40);
        if (arrayMap[tempPosX][tempPosY] == '-') {
          aSlimelist.add(aggressiveSlime(tempPosX, tempPosY));
        }
        arrayMap[tempPosX][tempPosY] = aSlimelist[i].getMapSymbol();
        exit = true;
      }
    }
    for (int i = 0; i < 5; i++) {
      bool exit = false;
      while (exit == false) {
        int tempPosX = randomInRange(0, 20);
        int tempPosY = randomInRange(0, 40);
        if (arrayMap[tempPosX][tempPosY] == '-') {
          healingPotions.add(healingPotion(tempPosX, tempPosY));
        }
        arrayMap[tempPosX][tempPosY] = healingPotions[i].getMapSymbol();
        exit = true;
      }
    }
  }

  void showMap() {
    setPlayer(playerPosX, playerPosY);
    updateMap();
    for (int h = 0; h < 40; h++) {
      for (int w = 0; w < 20; w++) {
        stdout.write(arrayMap[w][h]);
      }
      print('');
    }
  }

  void updateMap() {
    for (int h = 0; h < 40; h++) {
      for (int w = 0; w < 20; w++) {
        if (arrayMap[w][h] == 'P') {
          if (playerPosX != w || playerPosY != h) {
            arrayMap[w][h] = '-';
          }
        }
      }
    }
  }

  void setPlayer(int x, int y) {
    arrayMap[x][y] = 'P';
  }

  Future<bool> isMonsterAt(map m1, int posX, int posY) async {
    if (arrayMap[posX][posY] == 'N') {
      printRed("Approaching normal slime.");
      if (isAdvantage(m1.mapElement, m1.getMonsterElementByPos(posX, posY)) ==
          true) {
        m1.setMonsterMaxHpByPos(
            posX, posY, m1.getMonsterHpByPos(posX, posY) + 5);
      }
      getMonsterByPos(posX, posY, 'N');
      approachMonster = true;
      return true;
    } else if (arrayMap[posX][posY] == 'A') {
      printRed("Approaching aggressive slime.");
      if (m1.getMonsterElementByPos(posX, posY) == m1.mapElement) {
        m1.setMonsterMaxHpByPos(
            posX, posY, m1.getMonsterHpByPos(posX, posY) + 5);
      }
      getMonsterByPos(posX, posY, 'A');
      approachMonster = true;
      return true;
    } else if (arrayMap[posX][posY] == 'B') {
      printRed("Approaching boss.");
      if (m1.getMonsterElementByPos(posX, posY) == m1.mapElement) {
        m1.setMonsterMaxHpByPos(
            posX, posY, m1.getMonsterHpByPos(posX, posY) + 5);
      }
      getMonsterByPos(posX, posY, 'B');
      approachMonster = true;
      return true;
    } else {
      approachMonster = false;
      return false;
    }
  }

  bool isPotionAt(map m1, int posX, int posY) {
    if (arrayMap[posX][posY] == 'H') {
      m1.printGreen("Player HP restored!");
      return true;
    }
    return false;
  }

  String getMonsterSymbolByPos(int posX, int posY) {
    return arrayMap[posX][posY];
  }

  void getMonsterByPos(int posX, int posY, String monsterType) {
    switch (monsterType) {
      case 'N':
        {
          for (int i = 0; i < 5; i++) {
            if (nSlimelist[i]._posX == posX && nSlimelist[i]._posY == posY) {
              return nSlimelist[i].getStat();
            }
          }
        }
        break;
      case 'A':
        {
          for (int i = 0; i < 5; i++) {
            if (aSlimelist[i]._posX == posX && aSlimelist[i]._posY == posY) {
              return aSlimelist[i].getStat();
            }
          }
        }
        break;
      case 'B':
        {
          return boss.getStat();
        }
    }
  }

  int getMonsterHpByPos(int posX, int posY) {
    String monsterType = getMonsterSymbolByPos(posX, posY);
    switch (monsterType) {
      case 'N':
        {
          for (int i = 0; i < 5; i++) {
            if (nSlimelist[i]._posX == posX && nSlimelist[i]._posY == posY) {
              return nSlimelist[i].getHealthpoint();
            }
          }
        }
        break;
      case 'A':
        {
          for (int i = 0; i < 5; i++) {
            if (aSlimelist[i]._posX == posX && aSlimelist[i]._posY == posY) {
              return aSlimelist[i].getHealthpoint();
            }
          }
        }
        break;
      case 'B':
        {
          return boss.getHealthpoint();
        }
    }
    return 0;
  }

  String getMonsterElementByPos(int posX, int posY) {
    String monsterType = getMonsterSymbolByPos(posX, posY);
    switch (monsterType) {
      case 'N':
        {
          for (int i = 0; i < 5; i++) {
            if (nSlimelist[i]._posX == posX && nSlimelist[i]._posY == posY) {
              return nSlimelist[i].getElement();
            }
          }
        }
        break;
      case 'A':
        {
          for (int i = 0; i < 5; i++) {
            if (aSlimelist[i]._posX == posX && aSlimelist[i]._posY == posY) {
              return aSlimelist[i].getElement();
            }
          }
        }
        break;
      case 'B':
        {
          return boss.getElement();
        }
    }
    return "none";
  }

  void setMonsterMaxHpByPos(int posX, int posY, int value) {
    String monsterType = getMonsterSymbolByPos(posX, posY);
    switch (monsterType) {
      case 'N':
        {
          for (int i = 0; i < 5; i++) {
            if (nSlimelist[i]._posX == posX && nSlimelist[i]._posY == posY) {
              nSlimelist[i]
                  .setHealthpoint(nSlimelist[i].getHealthpoint() + value);
            }
          }
        }
        break;
      case 'A':
        {
          for (int i = 0; i < 5; i++) {
            if (aSlimelist[i]._posX == posX && aSlimelist[i]._posY == posY) {
              aSlimelist[i]
                  .setHealthpoint(aSlimelist[i].getHealthpoint() + value);
            }
          }
        }
        break;
      case 'B':
        {
          return boss.setHealthpoint(boss.getHealthpoint() + value);
        }
    }
  }

  void setMonsterHpByPos(int posX, int posY, int damage) {
    String monsterType = getMonsterSymbolByPos(posX, posY);
    switch (monsterType) {
      case 'N':
        {
          for (int i = 0; i < 5; i++) {
            if (nSlimelist[i]._posX == posX && nSlimelist[i]._posY == posY) {
              nSlimelist[i]
                  .setHealthpoint(nSlimelist[i].getHealthpoint() - damage);
            }
          }
        }
        break;
      case 'A':
        {
          for (int i = 0; i < 5; i++) {
            if (aSlimelist[i]._posX == posX && aSlimelist[i]._posY == posY) {
              aSlimelist[i]
                  .setHealthpoint(aSlimelist[i].getHealthpoint() - damage);
            }
          }
        }
        break;
      case 'B':
        {
          return boss.setHealthpoint(boss.getHealthpoint() - damage);
        }
    }
  }

  int getMonsterSpdByPos(int posX, int posY) {
    String monsterType = getMonsterSymbolByPos(posX, posY);
    switch (monsterType) {
      case 'N':
        {
          for (int i = 0; i < 5; i++) {
            if (nSlimelist[i]._posX == posX && nSlimelist[i]._posY == posY) {
              return nSlimelist[i].getBasicAtkSpd();
            }
          }
        }
        break;
      case 'A':
        {
          for (int i = 0; i < 5; i++) {
            if (aSlimelist[i]._posX == posX && aSlimelist[i]._posY == posY) {
              return aSlimelist[i].getBasicAtkSpd();
            }
          }
        }
        break;
      case 'B':
        {
          return boss.getBasicAtkSpd();
        }
    }
    return 0;
  }

  int getMonsterResByPos(var dmgType, int posX, int posY) {
    String monsterType = getMonsterSymbolByPos(posX, posY);
    switch (dmgType) {
      case 'P':
        {
          switch (monsterType) {
            case 'N':
              {
                for (int i = 0; i < 5; i++) {
                  if (nSlimelist[i]._posX == posX &&
                      nSlimelist[i]._posY == posY) {
                    return nSlimelist[i].getPhyRes();
                  }
                }
              }
              break;
            case 'A':
              {
                for (int i = 0; i < 5; i++) {
                  if (aSlimelist[i]._posX == posX &&
                      aSlimelist[i]._posY == posY) {
                    return aSlimelist[i].getPhyRes();
                  }
                }
              }
              break;
            case 'B':
              {
                return boss.getPhyRes();
              }
          }
        }
        break;
      case 'M':
        {
          switch (monsterType) {
            case 'N':
              {
                for (int i = 0; i < 5; i++) {
                  if (nSlimelist[i]._posX == posX &&
                      nSlimelist[i]._posY == posY) {
                    return nSlimelist[i].getMagicRes();
                  }
                }
              }
              break;
            case 'A':
              {
                for (int i = 0; i < 5; i++) {
                  if (aSlimelist[i]._posX == posX &&
                      aSlimelist[i]._posY == posY) {
                    return aSlimelist[i].getMagicRes();
                  }
                }
              }
              break;
            case 'B':
              {
                return boss.getMagicRes();
              }
          }
        }
    }
    return 0;
  }

  void setMonsterResByPos(var resType, int value, int posX, int posY) {
    String monsterType = getMonsterSymbolByPos(posX, posY);
    switch (resType) {
      case 'P':
        {
          switch (monsterType) {
            case 'N':
              {
                for (int i = 0; i < 5; i++) {
                  if (nSlimelist[i]._posX == posX &&
                      nSlimelist[i]._posY == posY) {
                    nSlimelist[i].setPhyRes(value);
                  }
                }
              }
              break;
            case 'A':
              {
                for (int i = 0; i < 5; i++) {
                  if (aSlimelist[i]._posX == posX &&
                      aSlimelist[i]._posY == posY) {
                    aSlimelist[i].setPhyRes(value);
                  }
                }
              }
              break;
            case 'B':
              {
                boss.setPhyRes(value);
              }
          }
        }
        break;
      case 'M':
        {
          switch (monsterType) {
            case 'N':
              {
                for (int i = 0; i < 5; i++) {
                  if (nSlimelist[i]._posX == posX &&
                      nSlimelist[i]._posY == posY) {
                    nSlimelist[i].setMagicRes(value);
                  }
                }
              }
              break;
            case 'A':
              {
                for (int i = 0; i < 5; i++) {
                  if (aSlimelist[i]._posX == posX &&
                      aSlimelist[i]._posY == posY) {
                    aSlimelist[i].setMagicRes(value);
                  }
                }
              }
              break;
            case 'B':
              {
                boss.setMagicRes(value);
              }
          }
        }
    }
  }

  int letMonsterAtkByPos(int posX, int posY) {
    String monsterType = getMonsterSymbolByPos(posX, posY);
    switch (monsterType) {
      case 'N':
        {
          for (int i = 0; i < 5; i++) {
            if (nSlimelist[i]._posX == posX && nSlimelist[i]._posY == posY) {
              return nSlimelist[i].critical();
            }
          }
        }
        break;
      case 'A':
        {
          for (int i = 0; i < 5; i++) {
            if (aSlimelist[i]._posX == posX && aSlimelist[i]._posY == posY) {
              return aSlimelist[i].critical();
            }
          }
        }
        break;
      case 'B':
        {
          return boss.critical();
        }
    }
    return 0;
  }

  bool isHpRestoreDropByPos(int posX, int posY) {
    String monsterType = getMonsterSymbolByPos(posX, posY);
    switch (monsterType) {
      case 'N':
        {
          for (int i = 0; i < 5; i++) {
            if (nSlimelist[i]._posX == posX && nSlimelist[i]._posY == posY) {
              return nSlimelist[i].restoreHpDrop();
            }
          }
        }
        break;
      case 'A':
        {
          for (int i = 0; i < 5; i++) {
            if (aSlimelist[i]._posX == posX && aSlimelist[i]._posY == posY) {
              return aSlimelist[i].restoreHpDrop();
            }
          }
        }
        break;
      case 'B':
        {
          return boss.restoreHpDrop();
        }
    }
    return false;
  }

  @override
  bool isAdvantage(String mapElement, String element) {
    if (mapElement == element) {
      printRed("This monster seems a bit stronger then usual!");
      return true;
    }
    return false;
  }
}

Future<void> main() async {
  print("SLIME SLAYER");
  await Future.delayed(const Duration(seconds: 2), () {});
  print("Select your slayer role to start playing");
  var roleList = Chooser<String>(
    ['Fighter', 'Mage', 'Markman'],
    message: 'Select your slayer role : ',
  );
  String role = roleList.chooseSync();
  print('You choose $role !');
  print("Starting... Please wait.");
  await Future.delayed(const Duration(seconds: 2), () {});
  await DriverMethod(role);
}

// ignore: non_constant_identifier_names
Future<void> DriverMethod(String role) async  {
  switch (role) {
    case "Fighter":
      {
        fighter f1 = fighter();
        while (f1.isDead() == false) {
          map m1 = map();
          f1.setPosX(19);
          f1.setPosY(39);
          m1.playerPosX = f1._posX;
          m1.playerPosY = f1._posY;
          while (f1.isDead() == false && m1.boss.isDead() == false) {
            print("Current player stat");
            f1.getStat();
            m1.showMap();
            String direction = stdin.readLineSync()!;
            f1.walk(direction);
            m1.playerPosX = f1._posX;
            m1.playerPosY = f1._posY;
            if (m1.isPotionAt(m1, m1.playerPosX, m1.playerPosY) == true) {
              f1.setHealthpoint(f1._maxHealthPoint);
            }
            m1.isMonsterAt(m1, m1.playerPosX, m1.playerPosY);
            while (m1.approachMonster == true && f1.isDead() == false) {
              // ignore: unused_local_variable
              int currentPlayerHp = f1.getHealthpoint();
              int monsterHp =
                  m1.getMonsterHpByPos(m1.playerPosX, m1.playerPosY);
              await Future.delayed(const Duration(seconds: 2), () {});
              if (f1.getBasicAtkSpd() <=
                      m1.getMonsterSpdByPos(m1.playerPosX, m1.playerPosY) &&
                  f1.isDead() == false) {
                await PlayerAtkTurn(f1: f1, m1);
                if (monsterHp > 0) {
                  await MonsterAtkTurn(f1: f1, m1);
                }
              } else if (f1.getBasicAtkSpd() >
                      m1.getMonsterSpdByPos(m1.playerPosX, m1.playerPosY) &&
                  monsterHp > 0) {
                await MonsterAtkTurn(f1: f1, m1);
                if (f1.isDead() == false) {
                  await PlayerAtkTurn(f1: f1, m1);
                }
              }
              if (m1.getMonsterHpByPos(m1.playerPosX, m1.playerPosY) <= 0) {
                await Future.delayed(const Duration(seconds: 2), () {});
                m1.approachMonster = false;
              }
            }
          }
          if (m1.boss.isDead() == true && f1.isDead() == false) {
            m1.printRed("Boss has been defeated.");
            print("You are in new map");
          }
        }
        print("\x1b[31mYou has been defeated.\x1B[0m");
      }
      break;
    case "Mage":
      {
        mage ma1 = mage();
        while (ma1.isDead() == false) {
          map m1 = map();
          ma1.setPosX(19);
          ma1.setPosY(39);
          m1.playerPosX = ma1._posX;
          m1.playerPosY = ma1._posY;
          while (ma1.isDead() == false && m1.boss.isDead() == false) {
            print("Current player stat");
            ma1.getStat();
            m1.showMap();
            String direction = stdin.readLineSync()!;
            ma1.walk(direction);
            m1.playerPosX = ma1._posX;
            m1.playerPosY = ma1._posY;
            // ignore: unrelated_type_equality_checks
            if (m1.isPotionAt(m1, m1.playerPosX, m1.playerPosY) == true) {
              ma1.setHealthpoint(ma1._maxHealthPoint);
            }
            m1.isMonsterAt(m1, m1.playerPosX, m1.playerPosY);
            while (m1.approachMonster == true && ma1.isDead() == false) {
              // ignore: unused_local_variable
              int currentPlayerHp = ma1.getHealthpoint();
              int monsterHp =
                  m1.getMonsterHpByPos(m1.playerPosX, m1.playerPosY);
              await Future.delayed(const Duration(seconds: 2), () {});
              if (ma1.getBasicAtkSpd() <=
                      m1.getMonsterSpdByPos(m1.playerPosX, m1.playerPosY) &&
                  ma1.isDead() == false) {
                await PlayerAtkTurn(ma1: ma1, m1);
                if (monsterHp > 0) {
                  await MonsterAtkTurn(ma1: ma1, m1);
                }
              } else if (ma1.getBasicAtkSpd() >
                      m1.getMonsterSpdByPos(m1.playerPosX, m1.playerPosY) &&
                  monsterHp > 0) {
                await MonsterAtkTurn(ma1: ma1, m1);
                if (ma1.isDead() == false) {
                  await PlayerAtkTurn(ma1: ma1, m1);
                }
              }
              if (m1.getMonsterHpByPos(m1.playerPosX, m1.playerPosY) <= 0) {
                await Future.delayed(const Duration(seconds: 2), () {});
                m1.approachMonster = false;
              }
            }
          }
          if (m1.boss.isDead() == true && ma1.isDead() == false) {
            m1.printRed("Boss has been defeated.");
            print("You are in new map");
          }
        }
        print("\x1b[31mYou has been defeated.\x1B[0m");
      }
      break;
    case "Markman":
      {
        markman mm1 = markman();
        while (mm1.isDead() == false) {
          map m1 = map();
          mm1.setPosX(19);
          mm1.setPosY(39);
          m1.playerPosX = mm1._posX;
          m1.playerPosY = mm1._posY;
          while (mm1.isDead() == false && m1.boss.isDead() == false) {
            print("Current player stat");
            mm1.getStat();
            m1.showMap();
            String direction = stdin.readLineSync()!;
            mm1.walk(direction);
            m1.playerPosX = mm1._posX;
            m1.playerPosY = mm1._posY;
            if (m1.isPotionAt(m1, m1.playerPosX, m1.playerPosY) == true) {
              mm1.setHealthpoint(mm1._maxHealthPoint);
            }
            m1.isMonsterAt(m1, m1.playerPosX, m1.playerPosY);
            while (m1.approachMonster == true && mm1.isDead() == false) {
              // ignore: unused_local_variable
              int currentPlayerHp = mm1.getHealthpoint();
              int monsterHp =
                  m1.getMonsterHpByPos(m1.playerPosX, m1.playerPosY);
              await Future.delayed(const Duration(seconds: 2), () {});
              if (mm1.getBasicAtkSpd() <=
                      m1.getMonsterSpdByPos(m1.playerPosX, m1.playerPosY) &&
                  mm1.isDead() == false) {
                await PlayerAtkTurn(mm1: mm1, m1);
                if (monsterHp > 0) {
                  await MonsterAtkTurn(mm1: mm1, m1);
                }
              } else if (mm1.getBasicAtkSpd() >
                      m1.getMonsterSpdByPos(m1.playerPosX, m1.playerPosY) &&
                  monsterHp > 0) {
                await MonsterAtkTurn(mm1: mm1, m1);
                if (mm1.isDead() == false) {
                  await PlayerAtkTurn(mm1: mm1, m1);
                }
              }
              if (m1.getMonsterHpByPos(m1.playerPosX, m1.playerPosY) <= 0) {
                await Future.delayed(const Duration(seconds: 2), () {});
                m1.approachMonster = false;
              }
            }
          }
          if (m1.boss.isDead() == true && mm1.isDead() == false) {
            m1.printRed("Boss has been defeated.");
            print("You are in new map");
          }
        }
        print("\x1b[31mYou has been defeated.\x1B[0m");
      }
  }
}

// ignore: non_constant_identifier_names
Future<void> PlayerAtkTurn(map m1, {fighter? f1, mage? ma1, markman? mm1}) async {
  if (f1 != null) {
    if (f1.getHealthpoint() <= 0) {
      return;
    }

    m1.printBlue("Player Attack Turn");
    await Future.delayed(const Duration(seconds: 2), () {});
    int canPlayerDealDmg = f1.critical();
    if (canPlayerDealDmg > 0) {
      int dealDmgToMonster = canPlayerDealDmg -
          m1.getMonsterResByPos('P', m1.playerPosX, m1.playerPosY);
      if (dealDmgToMonster > 0) {
        m1.setMonsterHpByPos(m1.playerPosX, m1.playerPosY, dealDmgToMonster);
        m1.printYellow("Player deal $dealDmgToMonster damage to monster");
        await Future.delayed(const Duration(seconds: 2), () {});
        if (f1.getMagicRes() > 0) {
          print("\x1b[47m\x1b[30mMonster physical resistence -1\x1b[0m");
          m1.setMonsterResByPos(
              'P',
              m1.getMonsterResByPos('P', m1.playerPosX, m1.playerPosY) - 1,
              m1.playerPosX,
              m1.playerPosY);
          int currentMonsterPhyRes =
              m1.getMonsterResByPos('P', m1.playerPosX, m1.playerPosY);
          print(
              "\x1b[47m\x1b[30mMonster physical resistence : $currentMonsterPhyRes\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        } else {
          print(
              "\x1b[47m\x1b[30mMonster physical resistence is completely broken!\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        }
      } else if (dealDmgToMonster <= 0) {
        print(
            "\x1b[47m\x1b[30mMonster completely blocked player damage!\x1b[0m");
        await Future.delayed(const Duration(seconds: 2), () {});
        if (f1.getMagicRes() > 0) {
          print("\x1b[47m\x1b[30mMonster physical resistence -1\x1b[0m");
          m1.setMonsterResByPos(
              'P',
              m1.getMonsterResByPos('P', m1.playerPosX, m1.playerPosY) - 1,
              m1.playerPosX,
              m1.playerPosY);
          int currentMonsterPhyRes =
              m1.getMonsterResByPos('P', m1.playerPosX, m1.playerPosY);
          print(
              "\x1b[47m\x1b[30mMonster physical resistence : $currentMonsterPhyRes\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        } else {
          print(
              "\x1b[47m\x1b[30mMonster physical resistence is completely broken!\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        }
      }
    } else if (canPlayerDealDmg == 0) {
      print("\x1b[47m\x1b[30mPlayer missed!\x1b[0m");
      await Future.delayed(const Duration(seconds: 2), () {});
    }
    int monsterHp = m1.getMonsterHpByPos(m1.playerPosX, m1.playerPosY);
    if (monsterHp > 0) {
      print("Monster HP : $monsterHp");
    } else {
      m1.printGreen("Monster is dead");
      await Future.delayed(const Duration(seconds: 2), () {});
      bool isHPRestoreDrop =
          m1.isHpRestoreDropByPos(f1.getPosX(), f1.getPosY());
      if (isHPRestoreDrop == true) {
        m1.printGreen("Player HP restored!");
        f1.setHealthpoint(f1._maxHealthPoint);
      }
    }
  } else if (ma1 != null) {
    if (ma1.getHealthpoint() <= 0) {
      return;
    }

    m1.printBlue("Player Attack Turn");
    await Future.delayed(const Duration(seconds: 2), () {});
    int canPlayerDealDmg = ma1.critical();

    if (canPlayerDealDmg > 0) {
      int dealDmgToMonster = canPlayerDealDmg -
          m1.getMonsterResByPos('P', m1.playerPosX, m1.playerPosY);
      if (dealDmgToMonster > 0) {
        m1.setMonsterHpByPos(m1.playerPosX, m1.playerPosY, dealDmgToMonster);
        m1.printYellow("Player deal $dealDmgToMonster damage to monster");
        await Future.delayed(const Duration(seconds: 2), () {});
        if (ma1.getMagicRes() > 0) {
          print("\x1b[47m\x1b[30mMonster magical resistence -1\x1b[0m");
          m1.setMonsterResByPos(
              'M',
              m1.getMonsterResByPos('M', m1.playerPosX, m1.playerPosY) - 1,
              m1.playerPosX,
              m1.playerPosY);
          int currentMonsterPhyRes =
              m1.getMonsterResByPos('M', m1.playerPosX, m1.playerPosY);
          print(
              "\x1b[47m\x1b[30mMonster magical resistence : $currentMonsterPhyRes\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        } else {
          print(
              "\x1b[47m\x1b[30mMonster magical resistence is completely broken!\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        }
      } else if (dealDmgToMonster <= 0) {
        print(
            "\x1b[47m\x1b[30mMonster completely blocked player damage!\x1b[0m");
        await Future.delayed(const Duration(seconds: 2), () {});
        if (ma1.getMagicRes() > 0) {
          print("\x1b[47m\x1b[30mMonster magical resistence -1\x1b[0m");
          m1.setMonsterResByPos(
              'P',
              m1.getMonsterResByPos('M', m1.playerPosX, m1.playerPosY) - 1,
              m1.playerPosX,
              m1.playerPosY);
          int currentMonsterPhyRes =
              m1.getMonsterResByPos('M', m1.playerPosX, m1.playerPosY);
          print(
              "\x1b[47m\x1b[30mMonster magical resistence : $currentMonsterPhyRes\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        } else {
          print(
              "\x1b[47m\x1b[30mMonster magical resistence is completely broken!\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        }
      }
    } else if (canPlayerDealDmg == 0) {
      print("\x1b[47m\x1b[30mPlayer missed!\x1b[0m");
      await Future.delayed(const Duration(seconds: 2), () {});
    }
    int monsterHp = m1.getMonsterHpByPos(m1.playerPosX, m1.playerPosY);
    if (monsterHp > 0) {
      print("Monster HP : $monsterHp");
    } else {
      m1.printGreen("Monster is dead");
      await Future.delayed(const Duration(seconds: 2), () {});
      bool isHPRestoreDrop =
          m1.isHpRestoreDropByPos(ma1.getPosX(), ma1.getPosY());
      if (isHPRestoreDrop == true) {
        m1.printGreen("Player HP restored!");
        ma1.setHealthpoint(ma1._maxHealthPoint);
      }
    }
  } else if (mm1 != null) {
    if (mm1.getHealthpoint() <= 0) {
      return;
    }

    m1.printBlue("Player Attack Turn");
    await Future.delayed(const Duration(seconds: 2), () {});
    int canPlayerDealDmg = mm1.critical();

    if (canPlayerDealDmg > 0) {
      int dealDmgToMonster = canPlayerDealDmg -
          m1.getMonsterResByPos('P', m1.playerPosX, m1.playerPosY);
      if (dealDmgToMonster > 0) {
        m1.setMonsterHpByPos(m1.playerPosX, m1.playerPosY, dealDmgToMonster);
        m1.printYellow("Player deal $dealDmgToMonster damage to monster");
        await Future.delayed(const Duration(seconds: 2), () {});
        if (mm1.getMagicRes() > 0) {
          print("\x1b[47m\x1b[30mMonster physical resistence -1\x1b[0m");
          m1.setMonsterResByPos(
              'P',
              m1.getMonsterResByPos('P', m1.playerPosX, m1.playerPosY) - 1,
              m1.playerPosX,
              m1.playerPosY);
          int currentMonsterPhyRes =
              m1.getMonsterResByPos('P', m1.playerPosX, m1.playerPosY);
          print(
              "\x1b[47m\x1b[30mMonster physical resistence : $currentMonsterPhyRes\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        } else {
          print(
              "\x1b[47m\x1b[30mMonster physical resistence is completely broken!\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        }
      } else if (dealDmgToMonster <= 0) {
        print(
            "\x1b[47m\x1b[30mMonster completely blocked player damage!\x1b[0m");
        await Future.delayed(const Duration(seconds: 2), () {});
        if (mm1.getMagicRes() > 0) {
          print("\x1b[47m\x1b[30mMonster physical resistence -1\x1b[0m");
          m1.setMonsterResByPos(
              'P',
              m1.getMonsterResByPos('P', m1.playerPosX, m1.playerPosY) - 1,
              m1.playerPosX,
              m1.playerPosY);
          int currentMonsterPhyRes =
              m1.getMonsterResByPos('P', m1.playerPosX, m1.playerPosY);
          print(
              "\x1b[47m\x1b[30mMonster physical resistence : $currentMonsterPhyRes\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        } else {
          print(
              "\x1b[47m\x1b[30mMonster physical resistence is completely broken!\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        }
      }
    } else if (canPlayerDealDmg == 0) {
      print("\x1b[47m\x1b[30mPlayer missed!\x1b[0m");
      await Future.delayed(const Duration(seconds: 2), () {});
    }
    int monsterHp = m1.getMonsterHpByPos(m1.playerPosX, m1.playerPosY);
    if (monsterHp > 0) {
      print("Monster HP : $monsterHp");
    } else {
      m1.printGreen("Monster is dead");
      await Future.delayed(const Duration(seconds: 2), () {});
      bool isHPRestoreDrop =
          m1.isHpRestoreDropByPos(mm1.getPosX(), mm1.getPosY());
      if (isHPRestoreDrop == true) {
        m1.printGreen("Player HP restored!");
        mm1.setHealthpoint(mm1._maxHealthPoint);
      }
    }
  }
}

// ignore: non_constant_identifier_names
Future<void> MonsterAtkTurn(map m1,{fighter? f1, mage? ma1, markman? mm1}) async {
  if (f1 != null) {
    if (m1.getMonsterHpByPos(m1.playerPosX, m1.playerPosY) <= 0) {
      return;
    }
    m1.printBlue("Monster Attack Turn");
    await Future.delayed(const Duration(seconds: 2), () {});
    int canMonsterDealDmg = m1.letMonsterAtkByPos(m1.playerPosX, m1.playerPosY);
    if (canMonsterDealDmg > 0) {
      int dealDmgToPlayer = canMonsterDealDmg - f1.getMagicRes();
      if (dealDmgToPlayer > 0) {
        f1.setHealthpoint(f1._healthpoint - dealDmgToPlayer);
        m1.printYellow("Monster deal $dealDmgToPlayer damage to player");
        await Future.delayed(const Duration(seconds: 2), () {});
        if (f1.getMagicRes() > 0) {
          print("\x1b[47m\x1b[30mPlayer magical resistence -1\x1b[0m");
          f1.setMagicRes(f1.getMagicRes() - 1);
          int currentPlayerMagicRes = f1.getMagicRes();
          print(
              "\x1b[47m\x1b[30mPlayer magical resistence : $currentPlayerMagicRes\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        } else {
          print(
              "\x1b[47m\x1b[30mPlayer physical resistence is completely broken!\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        }
      } else if (dealDmgToPlayer <= 0) {
        print(
            "\x1b[47m\x1b[30mPlayer completely blocked monster damage!\x1b[0m");
        await Future.delayed(const Duration(seconds: 2), () {});
        if (f1.getMagicRes() > 0) {
          print("\x1b[47m\x1b[30mPlayer magical resistence -1\x1b[0m");
          f1.setMagicRes(f1.getMagicRes() - 1);
          int currentPlayerMagicRes = f1.getMagicRes();
          print(
              "\x1b[47m\x1b[30mPlayer magical resistence : $currentPlayerMagicRes\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        } else {
          print(
              "\x1b[47m\x1b[30mPlayer physical resistence is completely broken!\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        }
      }
    } else if (canMonsterDealDmg == 0) {
      print("\x1b[47m\x1b[30mMonster missed!\x1b[0m");
      await Future.delayed(const Duration(seconds: 2), () {});
    }

    int playerHp = f1.getHealthpoint();
    if (playerHp > 0) {
      print("Player HP : $playerHp");
    } else {
      m1.printRed("Player is dead");
    }
  } else if (ma1 != null) {
    if (m1.getMonsterHpByPos(m1.playerPosX, m1.playerPosY) <= 0) {
      return;
    }
    m1.printBlue("Monster Attack Turn");
    await Future.delayed(const Duration(seconds: 2), () {});
    int canMonsterDealDmg = m1.letMonsterAtkByPos(m1.playerPosX, m1.playerPosY);
    if (canMonsterDealDmg > 0) {
      int dealDmgToPlayer = canMonsterDealDmg - ma1.getMagicRes();
      if (dealDmgToPlayer > 0) {
        ma1.setHealthpoint(ma1._healthpoint - dealDmgToPlayer);
        m1.printYellow("Monster deal $dealDmgToPlayer damage to player");
        await Future.delayed(const Duration(seconds: 2), () {});
        if (ma1.getMagicRes() > 0) {
          print("\x1b[47m\x1b[30mPlayer magical resistence -1\x1b[0m");
          ma1.setMagicRes(ma1.getMagicRes() - 1);
          int currentPlayerMagicRes = ma1.getMagicRes();
          print(
              "\x1b[47m\x1b[30mPlayer magical resistence : $currentPlayerMagicRes\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        } else {
          print(
              "\x1b[47m\x1b[30mPlayer physical resistence is completely broken!\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        }
      } else if (dealDmgToPlayer <= 0) {
        print(
            "\x1b[47m\x1b[30mPlayer completely blocked monster damage!\x1b[0m");
        await Future.delayed(const Duration(seconds: 2), () {});
        if (ma1.getMagicRes() > 0) {
          print("\x1b[47m\x1b[30mPlayer magical resistence -1\x1b[0m");
          ma1.setMagicRes(ma1.getMagicRes() - 1);
          int currentPlayerMagicRes = ma1.getMagicRes();
          print(
              "\x1b[47m\x1b[30mPlayer magical resistence : $currentPlayerMagicRes\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        } else {
          print(
              "\x1b[47m\x1b[30mPlayer physical resistence is completely broken!\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        }
      }
    } else if (canMonsterDealDmg == 0) {
      print("\x1b[47m\x1b[30mMonster missed!\x1b[0m");
      await Future.delayed(const Duration(seconds: 2), () {});
    }

    int playerHp = ma1.getHealthpoint();
    if (playerHp > 0) {
      print("Player HP : $playerHp");
    } else {
      m1.printRed("Player is dead");
    }
  } else if (mm1 != null) {
    if (m1.getMonsterHpByPos(m1.playerPosX, m1.playerPosY) <= 0) {
      return;
    }
    m1.printBlue("Monster Attack Turn");
    await Future.delayed(const Duration(seconds: 2), () {});
    int canMonsterDealDmg = m1.letMonsterAtkByPos(m1.playerPosX, m1.playerPosY);
    if (canMonsterDealDmg > 0) {
      int dealDmgToPlayer = canMonsterDealDmg - mm1.getMagicRes();
      if (dealDmgToPlayer > 0) {
        mm1.setHealthpoint(mm1._healthpoint - dealDmgToPlayer);
        m1.printYellow("Monster deal $dealDmgToPlayer damage to player");
        await Future.delayed(const Duration(seconds: 2), () {});
        if (mm1.getMagicRes() > 0) {
          print("\x1b[47m\x1b[30mPlayer magical resistence -1\x1b[0m");
          mm1.setMagicRes(mm1.getMagicRes() - 1);
          int currentPlayerMagicRes = mm1.getMagicRes();
          print(
              "\x1b[47m\x1b[30mPlayer magical resistence : $currentPlayerMagicRes\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        } else {
          print(
              "\x1b[47m\x1b[30mPlayer physical resistence is completely broken!\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        }
      } else if (dealDmgToPlayer <= 0) {
        print(
            "\x1b[47m\x1b[30mPlayer completely blocked monster damage!\x1b[0m");
        await Future.delayed(const Duration(seconds: 2), () {});
        if (mm1.getMagicRes() > 0) {
          print("\x1b[47m\x1b[30mPlayer magical resistence -1\x1b[0m");
          mm1.setMagicRes(mm1.getMagicRes() - 1);
          int currentPlayerMagicRes = mm1.getMagicRes();
          print(
              "\x1b[47m\x1b[30mPlayer magical resistence : $currentPlayerMagicRes\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        } else {
          print(
              "\x1b[47m\x1b[30mPlayer physical resistence is completely broken!\x1b[0m");
          await Future.delayed(const Duration(seconds: 2), () {});
        }
      }
    } else if (canMonsterDealDmg == 0) {
      print("\x1b[47m\x1b[30mMonster missed!\x1b[0m");
      await Future.delayed(const Duration(seconds: 2), () {});
    }

    int playerHp = mm1.getHealthpoint();
    if (playerHp > 0) {
      print("Player HP : $playerHp");
    } else {
      m1.printRed("Player is dead");
    }
  }
}
