
import 'dart:async';

import 'package:console/console.dart';

void progress(int progressPercent) {
  int i = 0;
  var progress = ProgressBar();
  Timer.periodic(const Duration(milliseconds: 25), (timer) {
    i++;
    progress.update(i);
    if (i >= progressPercent) {
      timer.cancel();
    }
  });
}
void main() {
 progress(0);
}